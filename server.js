const express = require('express');
const { join } = require('path');
const ajax = require('request');

const app = express();

const bingImageBase = 'https://www.bing.com';

app.set('view engine', 'ejs');
app.set('views', join(process.cwd(), 'pages'));
app.use(express.static(join(process.cwd(), 'public')));

app.get('/', function (req, response) {
  ajax('http://open.iciba.com/dsapi/', (error, resp, body) => {
    response.set('Cache-Control', 'max-age=3600');
    if (!error) {
      const json = JSON.parse(body);
      response.render('index', { image: json.fenxiang_img });
    } else {
      response.render('index', { image: '/images/main-bg.jpg' });
    }
  });
});

app.get('/daily-picture', function (req, response) {
  response.set('Cache-Control', 'max-age=3600');
  ajax('https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=zh-CN', function (error, resp, body) {
    const json = JSON.parse(body);
    const images = json.images;
    if (images.length === 0) {
      resp.send('No Image Available');
    } else {
      const image = images[0];
      response.render('daily-picture', {
        image: bingImageBase + image.url,
        time: image.enddate,
        copyright: {
          text: image.copyright,
          link: image.copyrightlink
        },
      });
    }
  });
});

app.listen(3000, function () {
  console.log("server started");
});